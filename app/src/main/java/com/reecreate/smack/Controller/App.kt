package com.reecreate.smack.Controller

import android.app.Application
import com.reecreate.smack.Utilities.SharedPrefs

/**
 * Created by reecreate on 20/09/2017.
 */
class App : Application() {

    companion object {
        lateinit var prefs: SharedPrefs
    }

    override fun onCreate() {
prefs = SharedPrefs(applicationContext)
        super.onCreate()
    }

}