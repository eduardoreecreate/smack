package com.reecreate.smack.Model

/**
 * Created by reecreate on 20/09/2017.
 */
class Channel(val name: String, val description: String, val id: String) {
    override fun toString(): String {
        return "#$name"
    }

}