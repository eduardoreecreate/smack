package com.reecreate.smack.Model

/**
 * Created by reecreate on 21/09/2017.
 */
class Message constructor(val message: String, val userName: String, val channelId: String,
                          val userAvatar: String, val userAvatarColor: String, val id: String,
                          val timeStamp: String)